﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gachkar_CMS.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Gachkar_CMS.Controller
{
    [Produces("application/json")]
    [Route("api/Blog")]
    public class BlogController : Microsoft.AspNetCore.Mvc.Controller
    {
        public IEnumerable<BlogPost> Get()
        {
            return new List<BlogPost>()
            {
                new BlogPost(){Guid = Guid.NewGuid(),Title = "First Blog",Content = "#My MD COntent \n\n LOOOL", CreationDate = DateTime.Now}
            };
        }
    }
}