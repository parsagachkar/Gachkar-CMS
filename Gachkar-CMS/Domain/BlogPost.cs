﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gachkar_CMS.Domain
{
    public class BlogPost:Entity
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
