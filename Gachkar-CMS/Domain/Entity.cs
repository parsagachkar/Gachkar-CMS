﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gachkar_CMS.Domain
{
    public class Entity
    {
        public Guid Guid { get; set; }
        public DateTime CreationDate { get; set; }
        
    }
}
