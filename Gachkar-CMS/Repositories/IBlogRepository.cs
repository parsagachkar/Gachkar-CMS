﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gachkar_CMS.Domain;

namespace Gachkar_CMS.Repositories
{
    public interface IBlogRepository
    {
        Task<IEnumerable<BlogPost>> GetBlogPosts();
        Task<IEnumerable<BlogPost>> GetBlogPosts(string[] tag);
        Task<IEnumerable<BlogPost>> GetBlogPost(Guid guid);
        Task NewBlogPost(BlogPost blogPost);
        Task EditBlogPost(BlogPost blogPost);
        Task DeleteBlogPost(Guid guid);
    }
}